from setuptools import setup, find_packages
import os

import epygma

setup(name="epygma",
      version=epygma.__VERSION__,
      keywords='Enigma Epygma Encrypt WWII',
      classifiers=["Topic :: Education",
                   "Topic :: Other/Nonlisted Topic",
                   "Topic :: Security :: Cryptography",
                   "Topic :: Security",
                   "Topic :: Sociology :: History",
                   "Topic :: System :: Emulators",
                   "Topic :: Utilities"],
      url="https://readthedocs.org/projects/epygma/",
      download_url="https://bitbucket.org/deusyss/epygma/overview",
      packages=find_packages(),
      install_requires=['jsonschema>=2.6'],
      # data_files=[("devices", [])],
      description="Settable Python simulator for WWII ENIGMA device",
      long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
      licence="GPL V3",
      plateformes='ALL')
