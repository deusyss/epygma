******
Epygma
******

The project
===========

https://bitbucket.org/deusyss/epygma/overview

This project was created at the beginning of the 2017 year.

.. TODO:: to complete

About the authors
=================

GALODE Alexandre
~~~~~~~~~~~~~~~~

Breizh Citizen (Fr)

Member of website Developpez.com, and Python expert. He works everyday on Python project for
various customers.
Passionates by technic and history, this project was an opportunity to conciliate both passions.

FOURNIS Erwan
~~~~~~~~~~~~~

Breizh Citizen (Fr)

History professor, expert in WWI, WWII and Napoleon periods.

Licence
=======

All the delivered code is under licence GPLV3. So you use code at your own risk.
Authors made for the better, using dedicated tools and serious research, to guarantee as better as possible
the best code quality and reporoduced the most exactly ENIGMA comportment.

Tools used
==========

To guarantee a optimal code quality, we used Pylint, and McCabe packages. All the code was written under
Pycharm Communauty IDE. ".idea" directory is present on the bitbucket repository.

About historical realism of ENIGMA devices, we made many research to be as precised as possible. Feel
free to contact us if you think there is a problem somewhere in the code.
