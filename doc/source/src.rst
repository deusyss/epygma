*******
Sources
*******

On the Web
==========

http://alexandre.goyon.pagesperso-orange.fr/Enigma.htm

https://callicode.fr/pydefis/Enigma/txt

https://en.wikipedia.org/wiki/Enigma_rotor_details

http://enigma.louisedade.co.uk/howitworks.html

http://eljjdx.canalblog.com/archives/2012/05/27/24326750.html

https://fr.wikipedia.org/wiki/Enigma_(machine)#Les_rotors

http://geekeries.org/2015/07/enigma-cryptographie-fonctionnement-et-implementation/

http://harold.thimbleby.net/enigma/figs/Enigma%20plugboard.JPG

http://math.pc.vh.free.fr/divers/machines/enigma.htm

http://nomis80.org/cryptographie/node19.html

http://py-enigma.readthedocs.io/en/latest/reference.html

https://repliqueenigma.wordpress.com/fonctionnement/fonctionnementrotation_rotors/

http://user.engineering.uiowa.edu/~ie181/Documents/EnigmaBackground.htm

https://www.apprendre-en-ligne.net/crypto/Enigma/

http://www.bibmath.net/crypto/index.php?action=affiche&quoi=debvingt/enigmafonc

http://www.bibmath.net/crypto/index.php?action=affiche&quoi=debvingt/enigmaguerre

https://www.codesandciphers.org.uk/enigma/enigma2.htm

http://www.commentcamarche.net/contents/205-cryptographie-enigma

http://www.cryptomuseum.com/crypto/enigma/wiring.htm

http://www.cryptomuseum.com/crypto/enigma/working.htm

http://www.enseignement.polytechnique.fr/profs/informatique/Jean-Jacques.Levy/00/pi/poupard/enigma.html

https://www.google.fr/search?q=enigma+rotor&ie=utf-8&oe=utf-8&client=firefox-b&gfe_rd=cr&ei=0kj4WIOyDOf-8AfsirHoCQ#q=enigma+rotor+rotation&start=10

Books
=====

.. TODO:: to complete
