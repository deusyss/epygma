****************
How Enigma Works
****************

.. TODO:: to complete

Before starting explanation, you could do a right click on pictures and ask to display picture to see it bigger.


Gloassary
---------

*  Scrambler: reflector + rotors + stator
*  Steckers: invert letters by pair
*  Stator: Static rotor which is used to convert alphabet/rotor. Named "EnTrittsWalze" in Deutch, resumed "ETW".
*  Rotor:
*  Reflector: rotor, static or not, which allows to make the signal back to rotors. Named "UmKehrWalze" in Deutsch, resumed "UKW".


Global work
-----------

.. image:: _static/basic_works2.png 
   :align: center

Global work is the following. User press a key. Electric signal pass through steckers (1) then go to the scrambler (2).
Signal pass through stator and rotors (3, 4, 5, 6), then pass back (7, 8, 9, 10). After what signal pass through again steckers (11).
Finally, signal is used to light on the right lamp.

More precise work
-----------------

.. image:: _static/basic_works.png 
   :align: center

User press a letter, "W" in our example. First it passes the stator. Here no convertion with it. In output, we get a "W". Then we pass through the rotor III. 
It converts table (read it from left to right) converts "W" to "C".Rotor II converts "C" into "M" then Rotor I convert "M" to "Z". Reflector converts "Z" to "T", and return it to Rotor I.

Rotor I converts "T" to "J" (convert table work from right to left), then Rotor II converts "J" to "Z", and Rotor II "Z" to "F". So the "W" was converted to "F".

At this moment, Rotor III turns. So if we press again "W", this time we'd get "W" converted to "O".

Now return to first schema, and try with "F" letter. In output, we get "W" letter. So Enigma can be reversed with the same start configuation.

Cran = notch
