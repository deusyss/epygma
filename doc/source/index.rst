.. Epygma documentation master file, created by
   sphinx-quickstart on Fri May 12 07:39:04 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Epygma's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   the_project
   enigma_historical
   how_it_works
   architecture
   code_doc
   src


