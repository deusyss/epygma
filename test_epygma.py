"""
Module allowing to test epygma package
"""

import unittest

import epygma

CONTENT = {"NAME": "1938 M3 ARMY",
           "YEAR": 1938,
           "AUTHOR": "GALODE Alexandre",
           "MAIL": "deusyss@yahoo.fr",
           "ORDER": ["ROTOR03", "ROTOR02", "ROTOR01", "REFLECTOR"],
           "SCRAMBLER":
           {
               "ROTOR01":
               {
                   "TYPE": "ROTOR",
                   "MODEL": "ROTOR_ENIGMA_I",
                   "STATIC": True,
                   "START": ""
               },
               "ROTOR02":
               {
                   "TYPE": "ROTOR",
                   "MODEL": "ROTOR_ENIGMA_II",
                   "STATIC": True,
                   "START": ""
               },
               "ROTOR03":
               {
                   "TYPE": "ROTOR",
                   "MODEL": "ROTOR_ENIGMA_III",
                   "STATIC": True,
                   "START": ""
               },
               "REFLECTOR":
               {
                   "TYPE": "REFLECTOR",
                   "MODEL": "REFLECTOR_B",
                   "STATIC": False
               }
           },
           "STECKERS": [["O", "W", "S0"], ["H", "L", "S1"]],
           "HISTORY": "A small text to explain the history of the device described ion this file"}


class EnigmaTest(unittest.TestCase):
    """
    Class which heritage from unittest
    """

    def test_device(self):
        """
        Simple basic case.
        """
        enigma_device = epygma.epygma.Epygma(CONTENT)
        self.assertEqual(enigma_device.convert("Hello world")[0], "PHTKQKQTTZ")

    def test_change_start(self):
        """
        Simple basic case for start letter on rotor.
        """
        enigma_device = epygma.epygma.Epygma(CONTENT)
        enigma_device.scrambler.list_rotors[0].set_start_letter("A")  # Rotor III
        enigma_device.scrambler.list_rotors[1].set_start_letter("B")  # Rotor II
        enigma_device.scrambler.list_rotors[2].set_start_letter("C")  # Rotor I
        self.assertEqual(enigma_device.convert("Hello world")[0], "RWFEPXGINN")

        enigma_device.scrambler.list_rotors[0].reset()  # Reset rotor by rotor
        enigma_device.scrambler.list_rotors[1].reset()
        enigma_device.scrambler.list_rotors[2].reset()
        enigma_device.scrambler.list_rotors[0].set_start_letter("A")  # Rotor III
        enigma_device.scrambler.list_rotors[1].set_start_letter("B")  # Rotor II
        enigma_device.scrambler.list_rotors[2].set_start_letter("C")  # Rotor I
        self.assertEqual(enigma_device.convert("RWFEPXGINN")[0], "HELLOWORLD")

    def test_change_start_n_rotation(self):
        """
        Simple basic case for start letter and rotation rotor
        """
        enigma_device = epygma.epygma.Epygma(CONTENT)
        enigma_device.scrambler.list_rotors[0].set_start_letter("P")  # Rotor III
        enigma_device.scrambler.list_rotors[1].set_start_letter("B")  # Rotor II
        enigma_device.scrambler.list_rotors[2].set_start_letter("C")  # Rotor I
        self.assertEqual(enigma_device.convert("Hello world")[0], "NSWYFYQAGT")

        enigma_device.scrambler.list_rotors[0].reset()  # Reset rotor by rotor
        enigma_device.scrambler.list_rotors[1].reset()
        enigma_device.scrambler.list_rotors[2].reset()
        enigma_device.scrambler.list_rotors[0].set_start_letter("P")  # Rotor III
        enigma_device.scrambler.list_rotors[1].set_start_letter("B")  # Rotor II
        enigma_device.scrambler.list_rotors[2].set_start_letter("C")  # Rotor I
        self.assertEqual(enigma_device.convert("NSWYFYQAGT")[0], "HELLOWORLD")

    def test_change_steckers(self):
        """
        Simple basic case for steckers.
        """
        enigma_device = epygma.epygma.Epygma(CONTENT)
        enigma_device.steckers.set("E", "r", "S3")
        self.assertEqual(enigma_device.convert("Hello world")[0], "PCTKQKQYTZ")

        enigma_device.scrambler.reset()  # Reset all the rotors
        self.assertEqual(enigma_device.convert("PCTKQKQYTZ")[0], "HELLOWORLD")

    def test_change_offset(self):
        """
        Simple basic case for offset ring (I.E. Ringstellung)
        """
        enigma_device = epygma.epygma.Epygma(CONTENT)
        enigma_device.scrambler.list_rotors[0].set_offset("B")  # Set offset on Rotor III
        self.assertEqual(enigma_device.convert("HELLO WORLD")[0], "SFPTEAANVR")

        enigma_device.scrambler.reset()  # Reset all the rotors
        enigma_device.scrambler.list_rotors[0].set_offset("B")  # Set offset on Rotor III
        self.assertEqual(enigma_device.convert("SFPTEAANVR")[0], "HELLOWORLD")


if __name__ == "__main__":
    unittest.main()
